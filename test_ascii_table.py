"""
"""
import pandas as pd

from ascii_table import AsciiTable


def test_simple():
    """
    """
    data = ["COL1 COL2 COL3 ",
            "VAL1xVAL2xVAL3x",
            "1111 2222 3333 "]
    table = AsciiTable(data[0])
    for row in data[1:]:
        table.add_row(row)
    assert table.df.equals(pd.DataFrame([['VAL1x', 'VAL2x', 'VAL3x'], ['1111 ', '2222 ', '3333 ']], columns=['COL1', 'COL2', 'COL3']))


def test_strip():
    """
    """
    data = [
            "COL1 COL2 COL3 ",
            "VAL1xVAL2xVAL3x",
            "1111 2222 3333 ",
            ]
    table = AsciiTable(data[0], strip=True)
    for row in data[1:]:
        table.add_row(row)
    assert table.df.equals(pd.DataFrame([['VAL1x', 'VAL2x', 'VAL3x'], ['1111', '2222', '3333']], columns=['COL1', 'COL2', 'COL3']))


def test_replace():
    """
    """
    data = [
            "C  1 C  2 C  3 ",
            "VAL1xVAL2xVAL3x",
            "1111 2222 3333 ",
            ]
    table = AsciiTable(data[0], replace=['C__1', 'C__2', 'C__3'])
    for row in data[1:]:
        table.add_row(row)
    assert table.df.equals(pd.DataFrame([['VAL1x', 'VAL2x', 'VAL3x'], ['1111 ', '2222 ', '3333 ']], columns=['C__1', 'C__2', 'C__3']))


def test_offset0():
    """
    """
    data = [
            "COL1 COL2 COL3 ",
            "VAL1VAL2xxVAL3x",
            "111122222233333",
            ]
    table = AsciiTable(data[0], offset=dict(COL2=(1, 0)))
    table.debug()
    for row in data[1:]:
        table.add_row(row)
    print(table.df)
    expected = pd.DataFrame([['VAL1', 'VAL2xx', 'VAL3x'], ['1111', '222222', '33333']], columns=['COL1', 'COL2', 'COL3'])
    print(expected)
    assert table.df.equals(expected)


def test_offset1():
    """
    """
    data = [
            "COL1 COL2 COL3 ",
            "VAL1VAL2xxxVAL3",
            "111122222223333",
            ]
    table = AsciiTable(data[0], offset=dict(COL2=(1, 1)))
    table.debug()
    for row in data[1:]:
        table.add_row(row)
    print(table.df)
    expected = pd.DataFrame([['VAL1', 'VAL2xxx', 'VAL3'], ['1111', '2222222', '3333']], columns=['COL1', 'COL2', 'COL3'])
    print(expected)
    assert table.df.equals(expected)


def test_use_case():
    """
    """
    data = [
            " FIX ID XCOORD YCOORD AK AK  NUM AK NAME LAST      ",
            "   ABCD  234.9  3000.9AK 9999   NAME    DATA       ",
            "AAAAAAXX-234.99-3000.9A2Z-12345 LONGNAMELASTTILLEND",
            "x000000xx11111xx22222xx3xx44444xx555555xx666666666x"
            ]
    table = AsciiTable(data[0], replace=['FIX_ID', 'AK__NUM', 'AK_NAME'], offset=dict(AK_NAME=(1, 0), LAST=(1, 0)), strip=True)
    table.debug()
    for row in data[1:]:
        table.add_row(row)
    print(table.df)
    expected = pd.DataFrame([
        ['ABCD', '234.9', '3000.9', 'AK', '9999', 'NAME', 'DATA'],
        ['AAAAAAXX', '-234.99', '-3000.9', 'A2Z', '-12345', 'LONGNAME', 'LASTTILLEND'],
        ['x000000x', 'x11111x', 'x22222x', 'x3x', 'x44444x', 'x555555x', 'x666666666x']],
        columns=['FIX_ID', 'XCOORD', 'YCOORD', 'AK', 'AK__NUM', 'AK_NAME', 'LAST'])
    print(expected)
    assert table.df.equals(expected)
