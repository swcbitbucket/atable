"""
Convert ascii tables from header positions into dataframes.
Assume the headers are only separated by space.
Som eheaders may need combining.
Find the columns based on the index of the header.
"""
import pandas as pd


class Column:
    """
    """

    def __init__(self, name, index):
        """
        """
        self.name = name
        assert name.isidentifier()
        self.index = index
        self.length = len(name)

    def __str__(self):
        """
        """
        return f'{self.name=} {self.index=} {self.length=}'


class AsciiTable:
    """
    """

    def __init__(self, raw_header: str, replace: list = None, offset: dict = None, strip: bool = False):
        """
        raw_header is the full string containing the column headers
        replace is a list of replcement headers eg 'FIX_ID' replaces 'FIX ID'.
        offset is a dict of columns to tuple of characters to borrow from adjacent columns.
        eg (1,1) would take 1 character from the column before and 1 from the column after
        eg (2,0) would take 1 character from the column before
        eg (0,1) woul dtake 1 character from the column after
        strip=True to strip each column
        """
        self.data = []
        self.strip = strip

        # first lets join any col headers
        header = raw_header[:]
        if replace:
            for key in replace:
                header = header.replace(key.replace('_', ' '), key)

        self.header = header
        columns = header.split()
        self.columns = columns

        # check for no dupes
        assert sorted(set(columns)) == sorted(columns)

        # need to find start position of each column
        col_list = []
        idx = 0
        name = ''
        for c in header:
            if c == ' ':
                if name:
                    col_list.append(Column(name, name_idx))
                    name = ''
            elif name:
                name += c
            else:
                name = c
                name_idx = idx
            idx += 1

        # adjust first col to start at 0
        col_list[0].index = 0
        # adjust end of each col to start of next col
        for idx, col in enumerate(col_list[:-1]):
            col_list[idx].length = col_list[idx+1].index - col_list[idx].index
        # adjust last col to end at the end of the string
        col_list[-1].length = len(header) - col_list[-1].index

        if offset:
            # Now adjust for offsets
            for name in offset:
                for idx, col in enumerate(col_list):
                    if col.name == name:
                        before, after = offset[name]
                        if before:
                            col_list[idx-1].length -= before
                            col_list[idx].index -= before
                            col_list[idx].length += before
                        if after:
                            col_list[idx+1].index += after
                            col_list[idx+1].length -= after
                            col_list[idx].length += after

        self.col_list = col_list

    def debug(self):
        """
        Handy for debug
        """
        char = 'A'
        row = []
        for c in self.col_list:
            print(c)
            row.append(char*c.length)
            char = chr(ord(char)+1)
        print(self.header.replace(' ', 'x'))
        print(''.join(row))

    def add_row(self, row):
        """
        Check the row is the right length
        Pick out the values from the string.
        Optionaly strip the values found.
        """
        assert len(row) == len(self.header)
        data = []
        for col in self.col_list:
            val = row[col.index:col.index+col.length]
            data.append(val.strip() if self.strip else val)
        self.data.append(data)

    @property
    def df(self):
        """
        A pandas DataFrame
        """
        return pd.DataFrame(self.data, columns=self.columns)
